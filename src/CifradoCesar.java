import java.util.Scanner;// import statements



public class CifradoCesar {
	/**
	 * Objete Scanner declarado global
	 */
	public static Scanner lector = new Scanner(System.in);

	/**
	 *
	 * @version 1-2019
	 * @author Dario Navarro
	 * @since 07-02-2020
	 * 
	 */
	
	// M�todos p�blicos
	
	/**
	 * Metod main controla si vol o no tornar a introduir una frase per codificar.
	 * @see CifradoCesar
	 * @param args Array d'objectes String
	 * 
	 */
	public static void main(String[] args) {
		
		/**
		 * Objete de tipo String per comprobar si vols seguir introduint frases.
		 */
		String tornarAJugar = "Si";
		/**
		 * Objete de tipo String on es guardara la frase codificada.
		 */
		String novaFrase = "";
				
		while(tornarAJugar.equals("Si")){
			
			novaFrase = Codificacio(PosicionAMover(),IntroducirFrase());
			
			System.out.println(novaFrase);
			novaFrase = "";
			
			System.out.println("Vol tornar a posar una altra frase?(Si) (No)");
			tornarAJugar = lector.nextLine();
		}
		
		

		
		
	}
	//M�todos privados
		/**
		 * 
		 * Metodo  Setter Valida la introduccio d'un numero y ho retorna.
		 * @see CifradoCesar
		 * @return Retorna un numero validat
		 */
	
	private static int ValidarNumero() {
		/**
		 * Variable que es retornara para assignar un numero correcte.
		 */
		int numeroValidado;
		
		try {
			numeroValidado = lector.nextInt();
			return numeroValidado;
		} catch (Exception e) {
			System.out.println("Has de introduir un numero:");
			lector.nextLine();
			return ValidarNumero();
		}
		
	}
	/**
	 * 
	 * Metodo  Setter Introdueix una frase y la retorna.
	 * @see CifradoCesar
	 * @return Retorna la frase per codificar
	 */
	private static String IntroducirFrase() {
		/**
		 * Objete de tipus String de la frase que vols codificar.
		 */
		String frase;
		
		System.out.println("Introducir frase a codificar");
		lector.nextLine();
		frase = lector.nextLine();
		return frase;
	}
	/**
	 * 
	 * Metodo  Setter Introdueix el valor de les posicions a moure.
	 * @see CifradoCesar
	 * @return Retorna un numero per moure la posicions de les lletres.
	 */
	private static int PosicionAMover() {
		/**
		 * Variable on es guardara la posicions a moure les lletres de la frase.
		 */
		int posicionesMover;
		
		System.out.println("Numero de posiciones a mover");
		posicionesMover = ValidarNumero();
		
		return posicionesMover;
	}
	/**
	 * 
	 * Metodo  Getter/Setter Codifica la frase pasada per parametres.
	 * @see CifradoCesar
	 * @param posicionesMover El valor que moura la posicio de les lletres.
	 * @param frase Frase que ha de codificar.
	 * @return La frase codificada.
	 * 
	 */
	public static String Codificacio(int posicionesMover,String frase) {
		/**
		 * Variable de tipus array amb el valors del abecedari en mayuscula
		 */
		char[] abecedari = {'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'};
		/**
		 * Variable de tipus array amb el valors del abecedari en minuscula
		 */
		char[] abecedarimin = {'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		/**
		 * Variable de tipus caracter que serveix per guardar els caracter de la frase y fer comprobacions
		 */
		char caracter;
		/**
		 * Variable que indica en que posicio del abecedari es troba el caracter de la frase.
		 */
		int posicionLetra;
		
		int x = 0;
		/**
		 * Variable de tipus boolean per indicar si ya ha trobat el caracter per seguir amb el seguent.
		 */
		boolean caracaterTrobat = false;
		
		/**
		 * Objete de tipus String on es guardara la frase codificada para ser retornada.
		 */
		String novaFrase = "";
		
		for(int i=0;i<frase.length();i++) {
			caracter = frase.charAt(i);
			caracaterTrobat = false;
			x = 0;
			while(x<abecedari.length-1 && caracaterTrobat == false ) {
				if (caracter == abecedari[x]) {
					posicionLetra = x;
					if (posicionLetra+posicionesMover>25) {
						posicionLetra = posicionLetra+posicionesMover-25;
					}else {
						posicionLetra += posicionesMover; 
					}
					caracter = abecedari[posicionLetra];
					novaFrase += Character.toString(caracter);
					caracaterTrobat = true;
					
				}else if(caracter == abecedarimin[x]) {
					posicionLetra = x;
					if (posicionLetra+posicionesMover>25) {
						posicionLetra = posicionLetra+posicionesMover-25;
					}else {
						posicionLetra += posicionesMover; 
					}
					caracter = abecedarimin[posicionLetra];
					novaFrase += Character.toString(caracter);
					caracaterTrobat = true;
					
				}
				x++;
			}
			if(caracaterTrobat == false) {
				novaFrase += Character.toString(caracter);
				caracaterTrobat = true;
			}
	
		}
		return novaFrase;
	}

}
