import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
	
@RunWith(Parameterized.class)

public class CifradoCesarTest {


	private String frase;
	private int numPos;
	private String resultado;
	
	public CifradoCesarTest (int numPos,String frase, String fraseCodificada) {
		this.frase = frase;
		this.numPos = numPos;
		this.resultado = fraseCodificada;
	}
	
	@Parameters
	public static Collection<Object[]> numeros() {
		return Arrays.asList(new Object[][] {
			{7, "Hola Julio Cesar","Ovsh Qcspv Jlzhy"},
			{9, "Voy a ir al cine esta tarde","Fxi j rb ju lrwn ncdj djbmn"},
			{12, "Els lleons s'han escapat del coliseu","Qxf xxqbzf f'tmz qfomcmg pqx obxufqh"},
			{5, "Un amigo de todos es una amigo de nadie","Zs frnlt ij ytitx jx zsf frnlt ij sfinj"},
			{10, "Elige un empleo que te guste y no tendras que trabajar ni un dia mas en la vida","Ovsqo fx owzvoy bfo eo qfdeo j xy eoxnckd bfo ecklktkc xs fx nsk wkd ox vk gsnk"},	
			{1, "No conozco la llave del exito, pero la llave del fracaso es tratar de agradar a todo el mundo","Op dpopzdp mb mmbwf efm fyjup, qfsp mb mmbwf efm gsbdbtp ft usbubs ef bhsbebs b upep fm nvoep"},
			{2,"Ya pienses que puedes o pienses que no puedes, estas en lo cierto.","Bc rkgpugu swg rwgfgu q rkgpugu swg pq rwgfgu, guvcu gp nq ekgtvq."},
			{24,"Ya pienses que puedes o pienses que no puedes, estas en lo cierto.","Xy ohdmrdr ptd otdcdr n ohdmrdr ptd mn otdcdr, drsyr dm kn bhdqsn."},

		});
		
	}
	@Test
	public void CifradoCesarTest() {
		String fraseCodificada = CifradoCesar.Codificacio(numPos,frase);
		assertEquals(resultado,fraseCodificada);
	}
}
